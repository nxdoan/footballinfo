//
//  ViewController.swift
//  FootballInfo
//
//  Created by john on 12/23/15.
//  Copyright © 2015 thiendiahoi. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITextFieldDelegate {

    // MARK: Properties
    @IBOutlet weak var txtURL: UITextField!
    @IBOutlet weak var lblRes: UILabel!
    
    // MARK: Commons
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.txtURL.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: Delegate
    func textFieldDidEndEditing(textField: UITextField) {
        self.txtURL.endEditing(true)
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        self.txtURL.resignFirstResponder()
        return true
    }
    
    @IBAction func request(sender: UIButton) {
        // The guard statement lets us check that the URL we’ve provided is valid.
        guard let url = NSURL(string: self.txtURL.text!) else {
            self.lblRes.text = "Error: cannot create URL"
            return
        }
        let urlRequest = NSURLRequest(URL: url)
        
        // Then we need an NSURLSession to use to send the request
        let config = NSURLSessionConfiguration.defaultSessionConfiguration()
        let session = NSURLSession(configuration: config)
        
        // Then create the data task
        let task = session.dataTaskWithRequest(urlRequest, completionHandler: { (data, response, error) in
            guard let responseData = data else {
                self.lblRes.text = "Error: did not receive data"
                return
            }
            guard error == nil else {
                self.lblRes.text = "error calling GET on /posts/1"
                return
            }
            
            // parse the result as JSON, since that's what the API provides
            let post: NSDictionary
            do {
                post = try NSJSONSerialization.JSONObjectWithData(responseData,
                    options: []) as! NSDictionary
            } catch  {
                print("error trying to convert data to JSON")
                return
            }
            // now we have the post, let's just print it to prove we can access it
            print("The post is: " + post.description)
            
            //self.lblRes.text = String(post.count)
            
            // the post object is a dictionary
            // so we just access the title using the "title" key
            // so check for a title and print it if we have one
            if let postTitle = post["region"] as? String {
                print("The title is: " + postTitle)
            }
        })
        
        // And finally send it (yes, this is an oddly named function)
        task.resume()
    }
}

