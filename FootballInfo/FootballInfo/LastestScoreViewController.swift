//
//  LastestScoreViewController.swift
//  FootballInfo
//
//  Created by john on 1/3/16.
//  Copyright © 2016 thiendiahoi. All rights reserved.
//

import UIKit

class LastestScoreViewController: UIViewController {

    @IBOutlet weak var lblLastestScore: UILabel!
    var txtLivescoreApi:String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.txtLivescoreApi = self.getUrl()
        self.getDataFromServer()
    }

    func getUrl() -> String {
        let dto = NSKeyedUnarchiver.unarchiveObjectWithFile(ServiceDTO.ArchiveURL.path!) as? ServiceDTO
        if let url = dto?.liveScoreUrl {
            return url
        }
        return ""
    }
    
    func getDataFromServer() {
        self.lblLastestScore.text = ""
        // The guard statement lets us check that the URL we’ve provided is valid.
        guard let url = NSURL(string: self.txtLivescoreApi) else {
            self.lblLastestScore.text = "Error: cannot create URL"
            return
        }
        let urlRequest = NSURLRequest(URL: url)
        
        // Then we need an NSURLSession to use to send the request
        let config = NSURLSessionConfiguration.defaultSessionConfiguration()
        let session = NSURLSession(configuration: config)
        
        // Then create the data task
        let task = session.dataTaskWithRequest(urlRequest, completionHandler: { (data, response, error) in
            guard let responseData = data else {
                self.lblLastestScore.text = "Error: did not receive data"
                return
            }
            guard error == nil else {
                self.lblLastestScore.text = "error calling GET on /posts/1"
                return
            }
            
            // parse the result as JSON, since that's what the API provides
            let post: NSDictionary
            do {
                post = try NSJSONSerialization.JSONObjectWithData(responseData,
                    options: []) as! NSDictionary
            } catch  {
                print("error trying to convert data to JSON")
                return
            }
            // check the error response
            var informRes = ""
            if (post.valueForKey("ERROR") as! NSString) == "OK" {
                // now we have the post, let's just print it to prove we can access it
                let matches = post.valueForKey("matches") as! NSArray
                for match in matches {
                    let obj = match as! NSDictionary
                    informRes += "\n" + String(obj.valueForKey("match_time") as! NSString) + " " + String(obj.valueForKey("match_localteam_name") as! NSString) + " " + String(obj.valueForKey("match_localteam_score") as! NSString) + " - " + String(obj.valueForKey("match_visitorteam_score") as! NSString) + " " + String(obj.valueForKey("match_visitorteam_name") as! NSString) + "(" + String(obj.valueForKey("match_timer") as! NSString) + ")"
                }
            } else {
                informRes = String(post.valueForKey("ERROR") as! NSString)
                    + "\nThe current IP: " + String(post.valueForKey("IP") as! NSString)
            }
            self.lblLastestScore.text = informRes
            print(informRes)
        })
        
        // And finally send it (yes, this is an oddly named function)
        task.resume()

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
