//
//  TeamDTO.swift
//  FootballInfo
//
//  Created by john on 12/27/15.
//  Copyright © 2015 thiendiahoi. All rights reserved.
//

import Foundation

class TeamDTO: NSObject, NSCoding {
    
    // MARK: Properties
    var post: String
    var club: String
    var points: String
    var status: String
    var lost: String
    var win: String
    var draw: String
    var recentForm: String
    
    // MARK: Constructors
    init(post: String, club: String, points: String, lost: String, win: String, draw: String, recentForm: String, status: String) {
        self.post = post
        self.club = club
        self.points = points
        self.status = status
        self.lost = lost
        self.win = win
        self.draw = draw
        self.recentForm = recentForm
    }
    
    // MARK: NSCoding
    func encodeWithCoder(aCoder: NSCoder) {
        aCoder.encodeObject(post, forKey: "post")
        aCoder.encodeObject(club, forKey: "club")
        aCoder.encodeObject(points, forKey: "points")
        aCoder.encodeObject(lost, forKey: "lost")
        aCoder.encodeObject(win, forKey: "win")
        aCoder.encodeObject(draw, forKey: "draw")
        aCoder.encodeObject(recentForm, forKey: "recentForm")
        aCoder.encodeObject(status, forKey: "status")
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        let post = aDecoder.decodeObjectForKey("post") as! String
        let club = aDecoder.decodeObjectForKey("club") as! String
        let points = aDecoder.decodeObjectForKey("points") as! String
        let lost = aDecoder.decodeObjectForKey("lost") as! String
        let win = aDecoder.decodeObjectForKey("win") as! String
        let draw = aDecoder.decodeObjectForKey("draw") as! String
        let recentForm = aDecoder.decodeObjectForKey("recentForm") as! String
        let status = aDecoder.decodeObjectForKey("status") as! String
        self.init(post: post, club: club, points: points, lost: lost, win: win, draw: draw, recentForm: recentForm, status: status)
    }
}