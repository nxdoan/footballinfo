//
//  ServiceDTO.swift
//  FootballInfo
//
//  Created by john on 12/27/15.
//  Copyright © 2015 thiendiahoi. All rights reserved.
//

import UIKit

class ServiceDTO: NSObject, NSCoding {
    // MARK: Property
    var standingsUrl: String
    var liveScoreUrl: String
    
    static let DocumentsDirectory = NSFileManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask).first!
    static let ArchiveURL = DocumentsDirectory.URLByAppendingPathComponent("servicedto")
    
    // MARK: Constructor
    init(standingsUrl: String, liveScoreUrl: String) {
        self.standingsUrl = standingsUrl
        self.liveScoreUrl = liveScoreUrl
        super.init()
    }
    
    // MARK: NSCoding protocols
    func encodeWithCoder(aCoder: NSCoder) {
        aCoder.encodeObject(self.standingsUrl, forKey: "standingsUrl")
        aCoder.encodeObject(self.liveScoreUrl, forKey: "liveScoreUrl")
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        var liveScoreUrl = ""
        if aDecoder.decodeObjectForKey("liveScoreUrl") != nil {
            liveScoreUrl = aDecoder.decodeObjectForKey("liveScoreUrl") as! String
        }
        var standingsUrl = ""
        if aDecoder.decodeObjectForKey("standingsUrl") != nil {
            standingsUrl = aDecoder.decodeObjectForKey("standingsUrl") as! String
        }
        self.init(standingsUrl: standingsUrl, liveScoreUrl: liveScoreUrl)
    }
}
