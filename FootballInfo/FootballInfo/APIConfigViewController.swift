//
//  ViewController.swift
//  FootballInfo
//
//  Created by john on 12/23/15.
//  Copyright © 2015 thiendiahoi. All rights reserved.
//

import UIKit

class APIConfigViewController: UIViewController, UITextFieldDelegate {

    // MARK: Properties
    @IBOutlet weak var txtURL: UITextField!
    @IBOutlet weak var lblRes: UILabel!
    @IBOutlet weak var txtLivescoreApi: UITextField!
    @IBOutlet weak var lblResLivescore: UILabel!
    
    // MARK: Commons
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.txtURL.delegate = self
        self.txtLivescoreApi.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: Delegate
    func textFieldDidEndEditing(textField: UITextField) {
        self.txtURL.endEditing(true)
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        self.txtURL.resignFirstResponder()
        return true
    }
    
    @IBAction func testLiveScoreApi(sender: AnyObject) {
        self.lblResLivescore.text = ""
        // The guard statement lets us check that the URL we’ve provided is valid.
        guard let url = NSURL(string: self.txtLivescoreApi.text!) else {
            self.lblRes.text = "Error: cannot create URL"
            return
        }
        let urlRequest = NSURLRequest(URL: url)
        
        // Then we need an NSURLSession to use to send the request
        let config = NSURLSessionConfiguration.defaultSessionConfiguration()
        let session = NSURLSession(configuration: config)
        
        // Then create the data task
        let task = session.dataTaskWithRequest(urlRequest, completionHandler: { (data, response, error) in
            guard let responseData = data else {
                self.lblRes.text = "Error: did not receive data"
                return
            }
            guard error == nil else {
                self.lblRes.text = "error calling GET on /posts/1"
                return
            }
            
            // parse the result as JSON, since that's what the API provides
            let post: NSDictionary
            do {
                post = try NSJSONSerialization.JSONObjectWithData(responseData,
                    options: []) as! NSDictionary
            } catch  {
                print("error trying to convert data to JSON")
                return
            }
            // check the error response
            var informRes = ""
            if (post.valueForKey("ERROR") as! NSString) == "OK" {
                // now we have the post, let's just print it to prove we can access it
                let matches = post.valueForKey("matches") as! NSArray
                for match in matches {
                    let obj = match as! NSDictionary
                    informRes += "\n" + String(obj.valueForKey("match_time") as! NSString) + " " + String(obj.valueForKey("match_localteam_name") as! NSString) + " " + String(obj.valueForKey("match_localteam_score") as! NSString) + " - " + String(obj.valueForKey("match_visitorteam_score") as! NSString) + " " + String(obj.valueForKey("match_visitorteam_name") as! NSString) + "(" + String(obj.valueForKey("match_timer") as! NSString) + ")"
                }
                // save
                let serviceDto = ServiceDTO(standingsUrl: (self.txtURL.text!), liveScoreUrl: (self.txtLivescoreApi.text!))
                let path = ServiceDTO.ArchiveURL.path
                let isSuccessfulSave = NSKeyedArchiver.archiveRootObject(serviceDto, toFile: path!)
                if !isSuccessfulSave {
                    print("Failed to save API url...")
                }
            } else {
                informRes = String(post.valueForKey("ERROR") as! NSString)
                    + "\nThe current IP: " + String(post.valueForKey("IP") as! NSString)
            }
            self.lblResLivescore.text = informRes
            print(informRes)
        })
        
        // And finally send it (yes, this is an oddly named function)
        task.resume()

    }
    
    @IBAction func request(sender: UIButton) {
        lblRes.text = ""
        // The guard statement lets us check that the URL we’ve provided is valid.
        guard let url = NSURL(string: self.txtURL.text!) else {
            self.lblRes.text = "Error: cannot create URL"
            return
        }
        let urlRequest = NSURLRequest(URL: url)
        
        // Then we need an NSURLSession to use to send the request
        let config = NSURLSessionConfiguration.defaultSessionConfiguration()
        let session = NSURLSession(configuration: config)
        
        // Then create the data task
        let task = session.dataTaskWithRequest(urlRequest, completionHandler: { (data, response, error) in
            guard let responseData = data else {
                self.lblRes.text = "Error: did not receive data"
                return
            }
            guard error == nil else {
                self.lblRes.text = "error calling GET on /posts/1"
                return
            }
            
            // parse the result as JSON, since that's what the API provides
            let post: NSDictionary
            do {
                post = try NSJSONSerialization.JSONObjectWithData(responseData,
                    options: []) as! NSDictionary
            } catch  {
                print("error trying to convert data to JSON")
                return
            }
            print(post.description)
            // check the error response
            var informRes = ""
            if (post.valueForKey("ERROR") as! NSString) == "OK" {
                // now we have the post, let's just print it to prove we can access it
                let teams = post.valueForKey("teams") as! NSArray
                for team in teams {
                    let obj = team as! NSDictionary
                    informRes += "\n" + String(obj.valueForKey("stand_team_name") as! NSString)
                }
                // save
                let serviceDto = ServiceDTO(standingsUrl: (self.txtURL.text!), liveScoreUrl: (self.txtLivescoreApi.text!))
                let path = ServiceDTO.ArchiveURL.path
                let isSuccessfulSave = NSKeyedArchiver.archiveRootObject(serviceDto, toFile: path!)
                if !isSuccessfulSave {
                    print("Failed to save meals...")
                }
                
            } else {
                informRes = String(post.valueForKey("ERROR") as! NSString)
                    + "\nThe current IP: " + String(post.valueForKey("IP") as! NSString)
            }
            self.lblRes.text = informRes
            print(informRes)
        })
        
        // And finally send it (yes, this is an oddly named function)
        task.resume()
    }
}

