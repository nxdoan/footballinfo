//
//  StandingTableViewController.swift
//  FootballInfo
//
//  Created by john on 12/27/15.
//  Copyright © 2015 thiendiahoi. All rights reserved.
//

import UIKit

class StandingTableViewController: UITableViewController {

    // MARK: Properties
    var teams = [TeamDTO]()
    @IBOutlet var tableview: UITableView!
    var dateFormatter = NSDateFormatter()
    
    // MARK: Commons
    func getDataFromLocal() -> Bool {
        let defaults = NSUserDefaults.standardUserDefaults()
        let decoded = defaults.objectForKey("PRE_STANDING_TABLE") as? NSData
        if decoded != nil {
            self.teams = NSKeyedUnarchiver.unarchiveObjectWithData(decoded!) as! [TeamDTO]
            self.tableview.reloadData()
            return true
        }
        self.alert("There is no data in local...")
        return false
    }
    
    func getDataFromServer() {
        let savedUrl : String! = StandingBLO.getUrl()
        guard let url = NSURL(string: savedUrl) else {
            self.alert("Error: cannot create URL")
            return
        }

        let urlRequest = NSURLRequest(URL: url)
        
        // we need an NSURLSession to use to send the request
        let config = NSURLSessionConfiguration.defaultSessionConfiguration()
        let session = NSURLSession(configuration: config)
        
        // create the data task
        let task = session.dataTaskWithRequest(urlRequest, completionHandler: { (data, response, error) in
            guard let responseData = data else {
                // get the saved data from the last time
                self.getDataFromLocal()
                return
            }
            guard error == nil else {
                self.alert("error calling GET on /posts/1")
                return
            }
            // parse the result as JSON, since that's what the API provides
            dispatch_async(dispatch_get_main_queue(), {
                do {
                    let post = try NSJSONSerialization.JSONObjectWithData(responseData,
                        options: []) as! NSDictionary
                    // check the error response
                    if (post.valueForKey("ERROR") as! NSString) == "OK" {
                        self.teams = [TeamDTO]()
                        // now we have the post, let's just print it to prove we can access it
                        let teamArray = post.valueForKey("teams") as! NSArray
                        for t in teamArray {
                            let obj = t as! NSDictionary
                            let teamDto = TeamDTO(post: String(obj.valueForKey("stand_position") as! NSString),
                                club: String(obj.valueForKey("stand_team_name") as! NSString),
                                points: String(obj.valueForKey("stand_points") as! NSString),
                                lost: String(obj.valueForKey("stand_overall_l") as! NSString),
                                win: String(obj.valueForKey("stand_overall_w") as! NSString),
                                draw: String(obj.valueForKey("stand_overall_d") as! NSString),
                                recentForm: String(obj.valueForKey("stand_recent_form") as! NSString),
                                status: String(obj.valueForKey("stand_status") as! NSString)
                            )
                            self.teams += [teamDto]
                        }
                    }
                } catch  {
                    self.alert("error trying to convert data to JSON")
                    return
                }
                
                // update "last updated" title for refresh control
                let now = NSDate()
                let updateString = "Last Updated at " + self.dateFormatter.stringFromDate(now)
                self.refreshControl?.attributedTitle = NSAttributedString(string: updateString)
                
                if self.refreshControl!.refreshing {
                    self.refreshControl!.endRefreshing()
                }
                self.tableview.reloadData()
                return
            })
        })
        
        // and finally send it (yes, this is an oddly named function)
        task.resume()
    }
    
    func alert(message: String) {
        let alertController = UIAlertController(title: "FootballInfo", message: message, preferredStyle: UIAlertControllerStyle.Alert)
        alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default,handler: nil))
        
        self.presentViewController(alertController, animated: true, completion: nil)
    }
    
    
    // MARK: Override
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getDataFromLocal()
        
        self.dateFormatter.dateStyle = NSDateFormatterStyle.ShortStyle
        self.dateFormatter.timeStyle = NSDateFormatterStyle.LongStyle
        
        // set up the refresh control
        self.refreshControl = UIRefreshControl()
        self.refreshControl!.attributedTitle = NSAttributedString(string: "Pull to refresh")
        self.refreshControl!.addTarget(self, action: "myRefreshFunc:", forControlEvents: UIControlEvents.ValueChanged)
        self.tableview.addSubview(refreshControl!)
        
        getDataFromServer()
        
        // set up custom buttons
        // 1
        let rightAddBarButtonItem:UIBarButtonItem = UIBarButtonItem(title: "Add", style: UIBarButtonItemStyle.Plain, target: self, action: "addTapped:")
        // 2
        let rightSearchBarButtonItem:UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Search, target: self, action: "searchTapped:")
        // 3
        self.navigationItem.setLeftBarButtonItems([rightAddBarButtonItem,rightSearchBarButtonItem], animated: true)
    }
    
    func addTapped(sender: AnyObject) {
        print("addTapped")
    }
    
    override func viewDidDisappear(animated: Bool) {
        print("viewDidDisapper")
        // save the [TeamDTO] into local
        let defaults = NSUserDefaults.standardUserDefaults()
        let encodedTeam = NSKeyedArchiver.archivedDataWithRootObject(self.teams)
        defaults.setObject(encodedTeam, forKey: "PRE_STANDING_TABLE")
        defaults.synchronize()
    }
    
    func myRefreshFunc(sender: AnyObject) {
        self.getDataFromServer()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return teams.count
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell:StandingTableViewCell = tableView.dequeueReusableCellWithIdentifier("StandingTableViewCell") as! StandingTableViewCell

        // Configure the cell...
        let team = teams[indexPath.row]
        cell.lblPos.text = String(team.post)
        cell.lblTeamName.text = team.status
        let imageFileName = team.club.stringByReplacingOccurrencesOfString(" ", withString: "")
        cell.imgTeam.image = UIImage(named: imageFileName.lowercaseString)
        cell.lblPoints.text = team.points
        
        return cell
    }

    /*
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        print("didSelectRowAtIndexPath")
        self.performSegueWithIdentifier("showView", sender: self)
    }
    */
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showview" {
            let detailViewController = segue.destinationViewController as! TeamDetailViewController
            // indexPath is set to the path that was tapped
            let indexPath = self.tableView.indexPathForSelectedRow
            // titleString is set to the title at the row in the objects array.
            let selectedTeam = self.teams[indexPath!.row]
            // the titleStringViaSegue property of NewViewController is set.
            detailViewController.team = selectedTeam
            self.tableView.deselectRowAtIndexPath(indexPath!, animated: true)
            
        }
    }
}
