//
//  StandingBLO.swift
//  FootballInfo
//
//  Created by john on 12/27/15.
//  Copyright © 2015 thiendiahoi. All rights reserved.
//

import Foundation

class StandingBLO {
    
    func getStandingTable(url: String) {
        makingRequest(url) { (post: NSDictionary) in
            let defaults = NSUserDefaults.standardUserDefaults()
            defaults.setObject(post, forKey: "DATA_TEST")
        }
    }
    
    class func getUrl() -> String {
        let dto = NSKeyedUnarchiver.unarchiveObjectWithFile(ServiceDTO.ArchiveURL.path!) as? ServiceDTO
        if let url = dto?.standingsUrl {
            return url
        }
        return ""
    }
    
    func makingRequest(inputUrl: String, completion: (NSDictionary) -> Void) {
        // guard statement lets us check that the URL we’ve provided is valid
        guard let url = NSURL(string: inputUrl) else {
            print("Error: cannot create URL")
            return
        }
        let urlRequest = NSURLRequest(URL: url)
        
        // we need an NSURLSession to use to send the request
        let config = NSURLSessionConfiguration.defaultSessionConfiguration()
        let session = NSURLSession(configuration: config)
        
        // create the data task
        let task = session.dataTaskWithRequest(urlRequest, completionHandler: { (data, response, error) in
            guard let responseData = data else {
                print("Error: did not receive data")
                return
            }
            guard error == nil else {
                print("error calling GET on /posts/1")
                return
            }
            // parse the result as JSON, since that's what the API provides
            do {
                let post = try NSJSONSerialization.JSONObjectWithData(responseData,
                    options: []) as! NSDictionary
                dispatch_async(dispatch_get_main_queue()) {
                    completion(NSDictionary(dictionary: post))
                }
            } catch  {
                print("error trying to convert data to JSON")
                return
            }
        })
        
        // and finally send it (yes, this is an oddly named function)
        task.resume()
    }
}