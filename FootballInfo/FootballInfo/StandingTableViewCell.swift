//
//  StandingTableViewCell.swift
//  FootballInfo
//
//  Created by john on 12/27/15.
//  Copyright © 2015 thiendiahoi. All rights reserved.
//

import UIKit

class StandingTableViewCell: UITableViewCell {

    // MARK: Properties
    
    @IBOutlet weak var lblPos: UILabel!
    @IBOutlet weak var imgTeam: UIImageView!
    @IBOutlet weak var lblTeamName: UILabel!
    @IBOutlet weak var lblPoints: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
