//
//  TeamDetailViewController.swift
//  FootballInfo
//
//  Created by john on 1/3/16.
//  Copyright © 2016 thiendiahoi. All rights reserved.
//

import UIKit

class TeamDetailViewController: UIViewController {

    // MARK: Properties
    @IBOutlet weak var lblTeamName: UILabel!
    
    @IBOutlet weak var lblLost: UILabel!
    @IBOutlet weak var lblWin: UILabel!
    @IBOutlet weak var lblDraw: UILabel!
    @IBOutlet weak var lblRecentForm: UILabel!
    @IBOutlet weak var imvTeam: UIImageView!
    
    
    var team: TeamDTO!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.lblTeamName.text = self.team.club
        self.lblDraw.text = self.team.draw
        self.lblLost.text = self.team.lost
        self.lblWin.text = self.team.win
        self.lblRecentForm.text = self.team.recentForm
        let imageFileName = team.club.stringByReplacingOccurrencesOfString(" ", withString: "")
        self.imvTeam.image = UIImage(named: imageFileName.lowercaseString)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    // MARK: - Navigation
    /*
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        print("prepare")
    }
    */

}
